﻿namespace Fur.DependencyInjection.Lifetimes.AsSelf
{
    /// <summary>
    /// 单一实例生存期服务依赖接口，注册为本身实例
    /// </summary>
    public interface ISingletonAsSelfLifetime { }

    /// <summary>
    /// 单一实例生存期服务依赖接口，注册为本身实例
    /// </summary>
    /// <typeparam name="T">泛型类型</typeparam>
    public interface ISingletonAsSelfLifetime<T> { }
}