﻿namespace Fur.Options
{
    /// <summary>
    /// 项目类型
    /// </summary>
    /// <remarks>
    /// <para>不同的项目类型添加的服务有差异</para>
    /// </remarks>
    public enum FurProjectOptions
    {
        /// <summary>
        /// WebApi 类型
        /// </summary>
        WebApi,

        /// <summary>
        /// Mvc类型
        /// </summary>
        Mvc
    }
}