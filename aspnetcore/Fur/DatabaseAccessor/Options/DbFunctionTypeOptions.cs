﻿namespace Fur.DatabaseAccessor.Options
{
    /// <summary>
    /// 数据库函数类型选项
    /// </summary>
    internal enum DbFunctionTypeOptions
    {
        /// <summary>
        /// 标量函数
        /// </summary>
        Scalar,

        /// <summary>
        /// 表值函数
        /// </summary>
        Table
    }
}