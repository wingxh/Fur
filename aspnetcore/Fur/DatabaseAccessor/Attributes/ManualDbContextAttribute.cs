﻿using System;

namespace Fur.DatabaseAccessor.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ManualDbContextAttribute : Attribute
    {
    }
}
