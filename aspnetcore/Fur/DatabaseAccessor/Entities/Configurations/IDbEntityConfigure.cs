﻿namespace Fur.DatabaseAccessor.Entities
{
    /// <summary>
    /// 实体配置依赖接口
    /// </summary>
    public interface IDbEntityConfigure { }
}