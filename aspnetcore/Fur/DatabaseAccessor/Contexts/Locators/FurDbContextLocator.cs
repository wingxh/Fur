﻿namespace Fur.DatabaseAccessor.Contexts
{
    /// <summary>
    /// 默认数据库上下文定位器
    /// </summary>

    public sealed class FurDbContextLocator : IDbContextLocator { }
}