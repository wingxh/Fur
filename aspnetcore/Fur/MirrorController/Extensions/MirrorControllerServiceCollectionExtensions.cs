﻿using Fur.MirrorController.Conventions;
using Fur.MirrorController.Providers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using System;
using System.Linq;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// 镜面控制器服务拓展
    /// </summary>
    public static class MirrorControllerServiceCollectionExtensions
    {
        /// <summary>
        /// 镜面控制器服务拓展方法
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <param name="configuration">配置选项</param>
        /// <returns>新的服务集合</returns>
        public static IServiceCollection AddFurMirrorControllers(this IServiceCollection services)
        {
            var partManager = services.FirstOrDefault(s => s.ServiceType == typeof(ApplicationPartManager)).ImplementationInstance as ApplicationPartManager
                ?? throw new InvalidOperationException($"`{nameof(AddFurMirrorControllers)}` must be invoked after `{nameof(MvcServiceCollectionExtensions.AddControllers)}`.");

            partManager.FeatureProviders.Add(new MirrorControllerFeatureProvider());
            services.Configure<MvcOptions>(options =>
            {
                options.Conventions.Add(new MirrorApplicationModelConvention());
            });

            return services;
        }

        /// <summary>
        /// 镜面控制器Mvc构建器拓展方法
        /// </summary>
        /// <param name="mvcBuilder">Mvc构建器</param>
        /// <param name="configuration">配置选项</param>
        /// <returns>新的Mvc构建器</returns>
        public static IMvcBuilder AddFurMirrorControllers(this IMvcBuilder mvcBuilder)
        {
            var partManager = mvcBuilder.Services.FirstOrDefault(s => s.ServiceType == typeof(ApplicationPartManager)).ImplementationInstance as ApplicationPartManager
                ?? throw new InvalidOperationException($"`{nameof(AddFurMirrorControllers)}` must be invoked after `{nameof(MvcServiceCollectionExtensions.AddControllers)}`.");

            partManager.FeatureProviders.Add(new MirrorControllerFeatureProvider());
            mvcBuilder.AddMvcOptions(options =>
            {
                options.Conventions.Add(new MirrorApplicationModelConvention());
            });

            mvcBuilder.AddNewtonsoftJson();

            return mvcBuilder;
        }
    }
}