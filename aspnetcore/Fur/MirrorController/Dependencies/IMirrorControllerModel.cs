﻿using Fur.DependencyInjection.Lifetimes;

namespace Fur.MirrorController.Dependencies
{
    /// <summary>
    /// 镜面控制器依赖接口
    /// </summary>
    public interface IMirrorControllerModel : ITransientLifetime { }
}