﻿namespace Fur.Application.Functions.Dtos
{
    public class TestDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}